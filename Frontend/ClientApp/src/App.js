import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import {Login} from './components/Login'
import { Registration } from './components/Registration';
import { Cabinet } from './components/Cabinet';
import { AddLot } from './components/AddLot';
import { EditUser } from './components/EditUser';
import {Users} from './components/Users';
import { Lot } from './components/Lot';
import './custom.css'

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
        <Route exact path='/' component={Home} />
        <Route exact path='/login' component={Login} />
        <Route exact path ='/registration' component={Registration}/>
        <Route exact path ='/cabinet' component={Cabinet}/>
        <Route exact path ='/add' component={AddLot}/>
        <Route exact path ='/users/:id' component={EditUser}/>
        <Route exact path ='/users' component={Users}/>
        <Route exact path ='/lot/:id' component={Lot}/>
      </Layout>
    );
  }
}
