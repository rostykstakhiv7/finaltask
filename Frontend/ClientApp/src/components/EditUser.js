import axios from 'axios';
import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
export class EditUser extends Component {
  static displayName = EditUser.name;

  constructor(props) {
    super(props);
    this.state={name: "",
    surname:"",
    phone:"",
    email:"",
    login:"",
  password:""}
  }
  componentDidMount(){
    if(localStorage["isAdmin"]===true||localStorage["UserID"]===this.props.match.params.id)
    {
    axios.get(`/Users/${this.props.match.params.id}`,{ headers: {"Authorization" : `Bearer ${localStorage["token"]}`}})
    .then(response=>{this.setState({
      name:response.data.name,
      surname:response.data.surname,
      phone:response.data.phone,
      email:response.data.email,
      login:response.data.login
    })}).catch(error=>alert(error.response.data));
  }
  }
  setName(n)
  {
    if(n.length<50)
    {
      this.setState({name:n})
    }
    else
    {
      alert("Name can't contain more than 150 symbols")
    }
  }
setSurname(n)
  {
    if(n.length<50)
    {
      this.setState({surname:n})
    }
    else
    {
      alert("Surname can't contain more than 150 symbols")
    }
  }
  setEmail(n)
  {
    this.setState({email:n})
  }
  setPassword(n)
  {
    this.setState({password:n})
  }
  setLogin(n)
  {
    if(n.length<50)
    {
      this.setState({login:n})
    }
    else
    {
      alert("Login can't contain more than 50 symbols")
    }
  }
  setPhone(n)
  {
    this.setState({phone:n})
  }

  Edit(e)
  {
      e.preventDefault();
      axios.put("/Users",
      {
        "id":this.props.match.params.id,
        "name": this.state.name,
        "surname": this.state.surname,
        "email": this.state.email,
        "phone": this.state.phone,
        "login":this.state.login,
        "password": this.state.password
    },{ headers: {"Authorization" : `Bearer ${localStorage["token"]}`}}).then(()=>{document.location.href="/"}).catch(error=>{alert(error.response.data)})
  }
  render () {
    return (
      <div>
      {(localStorage["isAdmin"]==="true"||localStorage["UserID"]===this.props.match.params.id)?<div className="EditUser">
    <div className="InputField">
    <h3>Name: </h3>
        <input type="text" id="name" value={this.state.name} onChange={e=>this.setName(e.target.value)} placeholder="Name"/>
    </div>
    <div className="InputField">
    <h3>Surname: </h3>
        <input type="text" id="surname" value={this.state.surname} onChange={e=>this.setSurname(e.target.value)} placeholder="Surname"/>
    </div>
    <div className="InputField">
    <h3>Email: </h3>
        <input type="email" id="email" value={this.state.email} onChange={e=>this.setEmail(e.target.value)} placeholder="Email"/>
    </div>
    <div className="InputField">
    <h3>Login: </h3>
        <input type="text" id="login" value={this.state.login} onChange={e=>this.setLogin(e.target.value)} placeholder="Login"/>
    </div>
    <div className="InputField">
    <h3>Phone: </h3>
        <input type="phone" id="phone" value={this.state.phone} onChange={e=>this.setPhone(e.target.value)} placeholder="Phone"/>
    </div>
    <div className="InputField">
    <h3>Confirm your password: </h3>
        <input type="password" id="phone" value={this.state.password} onChange={e=>this.setPassword(e.target.value)} placeholder="Enter your password"/>
    </div>    
        <button className="AddLotBtn" onClick={e=>{this.Edit(e)}}>Edit</button>
</div>:<div><Redirect to="/"></Redirect></div>}
     </div>
    );
  }
}