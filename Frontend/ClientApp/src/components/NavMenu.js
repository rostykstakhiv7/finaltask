import React, { Component } from 'react';
import { Container, Navbar, NavbarBrand, NavLink } from 'reactstrap';
import { Link } from 'react-router-dom';
import './NavMenu.css';

export class NavMenu extends Component {
  static displayName = NavMenu.name;

  setSearch(search) {
    localStorage.setItem("search", search)
  }
  Search() {
    document.location.href = "/";
  }
  Logout() {
    localStorage.clear();
    localStorage.setItem("search", "")
    document.location.href = "/login";
  }
  render() {
    return (
      <header>
        <Navbar className="navbar" light>
          <Container>
            <NavbarBrand  tag={Link} to="/"><h1 className='MainLink'>AutoParts</h1></NavbarBrand>

            <div className="search">
              <input
                type="text"
                placeholder="Enter name to find"
                name="search"
                onChange={e => this.setSearch(e.target.value)}
              />

              <button onClick={this.Search}>Search</button>
            </div>


            {localStorage["isAutorized"] ?
              <div className="IsLogin">
                <NavLink tag={Link} className="selection" to="/add">Add lot</NavLink>
                <NavLink tag={Link} className="selection" to="/cabinet">Cabinet</NavLink>
                <button onClick={this.Logout}>Logout</button>
              </div> :
              <NavLink tag={Link} className="selection" to="/login">Log In</NavLink>}
          </Container>
        </Navbar>
      </header>
    );
  }
}
