import axios from 'axios';
import React, { Component } from 'react';

export class Home extends Component {
  static displayName = Home.name;

  constructor(props) {
    super(props);
    this.state = { lots: [],
    sortby:"Name",
  sorttype:"asc",
limit:"10",
page:1,
isNext:true, 
count:0 };
    }
setSortBy(sort)
{
  this.setState({sortby:sort})
}

componentDidMount(){
  this.getParts(this.state.page)
}

addProduct(id){
  this.state.products.push(parseInt(id));
}
getParts(page)
{
  axios.get(`/Lots?s=${localStorage["search"]}&sort_by=${this.state.sortby}&sort_type=${this.state.sorttype}&offset=${page-1}&limit=${this.state.limit}`)
  .then(response=>{this.setState({lots:response.data.lots,count:response.data.count})}).then(()=>{
    this.setState({page:page})
    if((this.state.count-(this.state.page)*this.state.limit)>0)
    {
      this.setState({isNext:true})
      
    }
    else{
      this.setState({isNext:false})
    }
    localStorage.setItem("search","")
  }).then(()=>{
    if(this.state.lots.length>0)
    {
  document.getElementById('Lots').innerHTML="";
    }
  for(var i =0;i<this.state.lots.length;i++)
  {
    var image = document.createElement("img");
    image.setAttribute("src",require(`./pics/${this.state.lots[i].picturePath}`))
    var name = document.createElement("h3")
    name.innerText=this.state.lots[i].name
    var price = document.createElement("h3")
    price.innerText = "Price: "+this.state.lots[i].currentPrice
    console.log(this.state.lots[i]);
    var lot = document.createElement("div");
    var text = document.createElement("div");
    text.append(name, price);
    lot.setAttribute("class","Lot");
    lot.append(image,text)
    lot.setAttribute("id",`${this.state.lots[i].id}`)
    lot.onclick = (e)=>{
      if(e.target.localName!=="div")
      {document.location.href=`/lot/${e.target.parentElement.id}`}
      else{
        document.location.href=`/lot/${e.target.id}`
      }
  } 
    document.getElementById('Lots').append(lot)
  }
})
}
setSorttype(sort)
{
  this.setState({sorttype:sort})
}

setLimit(limit)
{
  this.setState({limit:limit})
}
Next()
{
  this.getParts(this.state.page+1)
  
}
Previous()
{
  this.getParts(this.state.page-1)
  
}
  

  render () {
    return (
      <div>
        {this.state.lots.length>0?<div class="MainFrame">
        <div className="Pagination">
          <h3>Sort: <select onChange={e=>{this.setSortBy(e.target.value)}}>
            <option value = "Name" >Name</option>
            <option value="CurrentPrice">Price</option>
          </select>
          <select onChange={e=>{this.setSorttype(e.target.value)}}>
            <option value = "asc" >Ascending</option>
            <option value="desc" >Descending</option>
          </select></h3>
         
          <h3>Limit: <input className="LimitInput" type="number" min={1}value={this.state.limit} onChange={e=>{this.setLimit(e.target.value)}}placeholder="limit"></input></h3>
          <button className = "filterButton" onClick={e=>{e.preventDefault();this.getParts(1)}}>Use Filters</button>
        </div>
        
        <div className="Lots" id="Lots">
        </div>
        {(this.state.page>1)&&<button className="NavigationButton" onClick={e=>{e.preventDefault();this.Previous()}}>previous</button>}
        {this.state.isNext&&<button className="NavigationButton" onClick={e=>{e.preventDefault();this.Next()}}>next</button>}
        </div>:<div><h1 className="Warning">We don't have lots for you, change search criterias or try again later</h1></div>}
        
      </div>
    );
  }
}
