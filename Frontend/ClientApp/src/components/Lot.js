import axios from 'axios';
import React, { Component } from 'react';
import pic from './pics/plugs.jpg';
export class Lot extends Component {
  static displayName = Lot.name;

  constructor(props) {
    super(props);
    this.state = {
      lotname: "",
      description: "",
      price: "",
      picture: "",
      name: "",
      surname: "",
      phone: "",
      email: "",
      isCustomer:false,
      isOwner:false,
      proposedPrice:0
    }
  }
  componentDidMount() {
    axios.get(`/Lots/${this.props.match.params.id}`, { headers: { "Authorization": `Bearer ${localStorage["token"]}` } })
      .then(response => {
        console.log(response); this.setState({
          lotname: response.data.name,
          description: response.data.description,
          price: response.data.currentPrice,
          picture: require("./pics/" + response.data.picturePath),
          name: response.data.owner.name,
          surname: response.data.owner.surname,
          phone: response.data.owner.phone,
          email: response.data.owner.email,
          isCustomer:response.data.customerId===parseInt(localStorage["UserID"]),
          isOwner:response.data.ownerId===parseInt(localStorage["UserID"])
        })
      }).catch(error => alert(error.response.data));
  }
  setPrice(p) {
    if (p >= 0) {
      this.setState({ proposedPrice: p })
    }
    else {
      alert("Price must be greater than 0")
    }
  }

  Buy(e) {
    e.preventDefault();
    if(localStorage["isAutorized"]==="true")
    {
      axios.post("/Orders",
      {
        "itemId": parseInt(this.props.match.params.id),
        "userId":parseInt(localStorage["UserID"]),
        "offeredPrice":parseInt(this.state.proposedPrice)
      }, { headers: { "Authorization": `Bearer ${localStorage["token"]}` } }).then(() => { document.location.href = "/" }).catch(error => { alert(error.response.data) })
    }
    else{
      document.location.href="/login"
    }

  }
  Cancel(e)
  {
    e.preventDefault()
    axios.delete(`/Orders/${localStorage["UserID"]}/${this.props.match.params.id}`,
     { headers: { "Authorization": `Bearer ${localStorage["token"]}` } })
     .then(() => { document.location.href = "/" }).catch(error => { alert(error.response.data) })
  }
  RemoveLot(e)
  {
    e.preventDefault()
    axios.delete(`/Lots/${this.props.match.params.id}`,
     { headers: { "Authorization": `Bearer ${localStorage["token"]}` } })
     .then(() => { document.location.href = "/" }).catch(error => { alert(error.response.data) })
  }
  render() {
    return (
      <div className="EditUser">
        <h1>{this.state.lotname}</h1>

        <div className='LotDetail'>

          <img className="LotPicture" src={this.state.picture}></img>
          <div><div className='Owner'>
            <h3>{this.state.name} {this.state.surname}</h3>
            <h3>{this.state.phone}</h3>
            <h3>{this.state.email}</h3>
          </div>
            <h3>Price: {this.state.price}</h3>
            <div><h3>Proposed price: </h3>
              <input type="number" className="ProposedPriceInput" id="price" value={this.state.proposedPrice} onChange={e => this.setPrice(e.target.value)} placeholder="Price" /></div>           
              <button className='BuyButton' onClick={e=>{this.Buy(e)}} >Buy</button>
              {this.state.isCustomer&&<button className='DeleteButton' onClick={e=>{this.Cancel(e)}}>Cancel order</button>}
              {(this.state.isOwner||localStorage["isAdmin"]==="true")&&<button className='DeleteButton' onClick={e=>{this.RemoveLot(e)}}>Remove Lot</button>}
              </div>
              
        </div>
        <h3>Description:</h3>
        <h3>{this.state.description}</h3>



      </div>
    );
  }
}