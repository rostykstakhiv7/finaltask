import axios from 'axios';
import React, { Component } from 'react';

export class AddLot extends Component {
  static displayName = AddLot.name;

  constructor(props) {
    super(props);
    this.state={name: "",
    description:"",
    price:"",
    picture:"noimage.jpg"}
  }
  setName(n)
  {
    if(n.length<150)
    {
      this.setState({name:n})
    }
    else
    {
      alert("Name can't contain more than 150 symbols")
    }
  }
  setDescription(d)
  {
    if(d.length<1000)
    {
      this.setState({description:d})
    }
    else
    {
      alert("Description can't contain more than 1000 symbols")
    }
  }
  setPrice(p)
  {
    if(p>=0)
    {
      this.setState({price:p})
    }
    else{
      alert("Price must be greater than 0")
    }
  }

  setPicture(p)
  {
      this.setState({picture:p.replace("C:\\fakepath\\","")})
  }
  Add(e)
  {
      e.preventDefault();
      axios.post("/Lots",
      {
        "picturePath": this.state.picture,
        "startPrice": parseInt(this.state.price),
        "currentPrice": parseInt(this.state.price),
        "ownerId": parseInt(localStorage["UserID"]),
        "customerId":null,
        "name": this.state.name,
        "description": this.state.description
    },{ headers: {"Authorization" : `Bearer ${localStorage["token"]}`}}).then(()=>{document.location.href="/"}).catch(error=>alert(error.response.data))
  }
  render () {
    return (
      <div className="AddLot">
        <div className="InputField">
        <h3>Name: </h3>
            <input type="text" id="name" value={this.state.name} onChange={e=>this.setName(e.target.value)} placeholder="Name"/>
        </div>
            
            <div className="InputField">
              <h3>Description: </h3>
            <textarea id="description" value={this.state.description} onChange={e=>this.setDescription(e.target.value)} placeholder="Description"/>
            </div>
            
            <div className="InputField"><h3>Price: </h3>
            <input type="number" id="price"  value={this.state.price} onChange={e=>this.setPrice(e.target.value)} placeholder="Price"/></div>
            
            <div className="InputField"><h3>Picture: </h3>
            <input type="file" id="file" onChange={e=>this.setPicture(e.target.value)} accept=".jpg, .jpeg, .png"/></div>
            
            <button className="AddLotBtn" onClick={e=>{this.Add(e)}}>Add Part</button>
    </div>
    );
  }
}
