import React, { Component } from 'react';
import axios  from 'axios'
export class Cabinet extends Component {
  constructor(props) {
    super(props);
    this.state = { ownlots: [],
      purchuasedlots:[]
    };
    }

  componentDidMount(){
    axios.get(`/Lots/mylots`,{ headers: {"Authorization" : `Bearer ${localStorage["token"]}`}})
  .then(response=>{
    this.setState({ownlots:response.data})
      this.state.ownlots.forEach(element => {
      var image = document.createElement("img");
      image.setAttribute("src",require(`./pics/${element.picturePath}`))
      var name = document.createElement("h3")
      name.innerText=element.name
      var price = document.createElement("h3")
      price.innerText = "Price: "+element.currentPrice
      var lot = document.createElement("div");
      var text = document.createElement("div");
      text.append(name, price);
      lot.setAttribute("class","Lot");
      lot.append(image,text)
      lot.setAttribute("id",`${element.id}`)
      lot.onclick = (e)=>{
        if(e.target.localName!=="div")
        {document.location.href=`/lot/${e.target.parentElement.id}`}
        else{
          document.location.href=`/lot/${e.target.id}`
        }
    } 
      document.getElementById('OwnLots').append(lot)
    });
  })

  axios.get(`/Lots/purchuasedlots`,{ headers: {"Authorization" : `Bearer ${localStorage["token"]}`}})
  .then(response=>{
    this.setState({purchuasedlots:response.data})
      this.state.purchuasedlots.forEach(element => {
      var image = document.createElement("img");
      image.setAttribute("src",require(`./pics/${element.picturePath}`))
      var name = document.createElement("h3")
      name.innerText=element.name
      var price = document.createElement("h3")
      price.innerText = "Price: "+element.currentPrice
      var lot = document.createElement("div");
      var text = document.createElement("div");
      text.append(name, price);
      lot.setAttribute("class","Lot");
      lot.append(image,text)
      lot.setAttribute("id",`${element.id}`)
      lot.onclick = (e)=>{
        if(e.target.localName!=="div")
        {document.location.href=`/lot/${e.target.parentElement.id}`}
        else{
          document.location.href=`/lot/${e.target.id}`
        }
    } 
      document.getElementById('PurchuasedOrders').append(lot)
    });
  })
  }
  render () {
    return (
      <div className="cabinet">
       <h1 className="Warning">You are logged as {localStorage["nickname"]}</h1>
       <button className="EditProfile" onClick={e=>{e.preventDefault();document.location.href=`/users/${localStorage["UserID"]}`}}>Edit profile</button>
       {this.state.purchuasedlots.length>0?<div><h1>Purchuased lots:</h1>
       <div className="CabinetLots" id="PurchuasedOrders">
       </div></div>:<div><h1 className="Warning">You don't have purchuased lots</h1></div>}
       
       {this.state.ownlots.length>0?<div><h1>Your lots:</h1>
       <div className="CabinetLots" id="OwnLots">
       </div></div>:<div><h1 className="Warning">You don't have own lots</h1></div>}
       
       
       {(localStorage["isAdmin"]==="true")&&<div><button className='UserRedirect' onClick={e=>{e.preventDefault();document.location.href="/users"}}>Users</button>
       </div>}
       </div>
    );
  }
}