using Data.Entities;
using Data.Interfaces;
using Data.Repositories;
using System;
using System.Threading.Tasks;

namespace Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly TestsDbContext _context;
        private IQuestionRepository questionRepository;
        private IRepository<User> userRepository;
        private IRepository<Test> testRepository;
        private IRepository<Role> roleRepository;
        private IRepository<Answer> answerRepository;
        public UnitOfWork(TestsDbContext context)
        {
            _context = context;
        }

        public IQuestionRepository QuestionRepository
        {
            get
            {
                if(questionRepository==null)
                {
                    questionRepository = new QuestionRepository(_context);
                }
                return questionRepository;
            }
        }

        public IRepository<User> UserRepository
        {
            get
            {
                if (userRepository == null)
                {
                    userRepository = new Repository<User>(_context);
                }
                return userRepository;
            }
        }


        public IRepository<Test> TestRepository
        {
            get
            {
                if (testRepository == null)
                {
                    testRepository = new Repository<Test>(_context);
                }
                return testRepository;
            }
        }

        public IRepository<Role> RoleRepository
        {
            get
            {
                if (roleRepository == null)
                {
                    roleRepository = new Repository<Role>(_context);
                }
                return roleRepository;
            }
        }

        public IRepository<Answer> AnswerRepository
        {
            get
            {
                if (answerRepository == null)
                {
                    answerRepository = new Repository<Answer>(_context);
                }
                return answerRepository;
            }
        }

        public async Task<int> SaveAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}