﻿namespace Data.Entities
{
    public class BaseClass
    {
        private int id;

        public int Id
        {
            set
            {
                id = value;
            }
            get => id;

        }
    }
}