﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Business.Validations
{
    public class AuctionException : Exception
    {
        public AuctionException()
        {
        }

        public AuctionException(string message) : base(message)
        {
        }

        public AuctionException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected AuctionException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
