﻿using AutoMapper;
using Business.Interfaces;
using Business.Models;
using Business.Validations;
using Data;
using Data.Entities;
using Data.Interfaces;
using Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{
    public class TestService : ITestService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly Random _random;
        public TestService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _random = new Random();
        }

        public async Task AddAsync(TestModel model)
        {
            await _unitOfWork.TestRepository.AddAsync(_mapper.Map<Test>(model));
            await _unitOfWork.SaveAsync();
        }

        public async void CheckTestAsync(TestModel test)
        {
            double totalMark = 0;
            double mark = 0;
            foreach (var question in test.Questions)
            {
                var answer = _unitOfWork.AnswerRepository.FindAll().FirstOrDefault(x => x.QuestionId == question.Id);
                if (answer == null)
                {
                    mark += question.Mark;
                }
                else
                {
                    if (question.Type == QuestionType.single)
                    {
                        if (question.Answers == answer.Answers)
                        {
                            mark += question.Mark;
                        }

                    }
                    else if (question.Type == QuestionType.wordfree)
                    {

                        if (answer.Answers.Contains(question.Answers[0]))
                        {
                            mark += question.Mark;
                        }
                    }
                    else
                    {
                        var partMark = question.Mark / answer.Answers.Count();
                        foreach (var item in question.Answers)
                        {
                            if(answer.Answers.Contains(item))
                            {
                                mark += partMark;
                            }
                            else
                            {
                                mark -= partMark;
                            }
                        }
                    }   
                }
                totalMark += question.Mark;
            }
            test.Mark = test.MaxMark * (mark / totalMark) * (0.4 + 0.2 * test.DificultyLevel);
            test.NumberOfAttempts -= 1;
            await UpdateAsync(test);
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            try
            {
                await _unitOfWork.TestRepository.DeleteByIdAsync(modelId);
                await _unitOfWork.SaveAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<TestModel> GenerateTestAsync(int testId, int difficultyLevel)
        {
            var test = await _unitOfWork.TestRepository.GetByIdAsync(testId);
            test.DificultyLevel = difficultyLevel;
            var questions = _unitOfWork.QuestionRepository.FindAll().Where(x => x.OwnerId == test.OwnerId
            && x.Subject == test.Subject && x.DificultyLevel <= difficultyLevel).ToList();
            test.Questions.Clear();
            foreach (var themeCount in test.ThemeCount)
            {
                var themeQuestions = questions.Where(x => x.Theme == themeCount.Key).ToList();
                for (int i = 0; i < themeCount.Value; i++)
                {
                    var k = _random.Next(themeQuestions.Count);
                    test.Questions.Add(themeQuestions[k]);
                    themeQuestions.RemoveAt(k);
                }
            }
            return _mapper.Map<TestModel>(test);
        }

        public IEnumerable<TestModel> GetAll()
        {
            return _mapper.Map<List<TestModel>>(_unitOfWork.TestRepository.FindAll());
        }

        public IEnumerable<TestModel> GetAllForUser(int userId)
        {
            var result = _unitOfWork.TestRepository.FindAll().Where(x => x.UserId == userId);
            return _mapper.Map<List<TestModel>>(result);
        }

        public async Task<TestModel> GetByIdAsync(int id)
        {
            var lot = await _unitOfWork.TestRepository.GetByIdAsync(id);
            if (lot == null)
            {
                throw new ArgumentNullException();
            }

            return _mapper.Map<TestModel>(lot);
        }

        public async Task UpdateAsync(TestModel model)
        {

            _unitOfWork.TestRepository.Update(_mapper.Map<Test>(model));
            await _unitOfWork.SaveAsync();
        }
    }
}
