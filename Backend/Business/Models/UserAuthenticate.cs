﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    public class UserAuthenticate
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }

}
