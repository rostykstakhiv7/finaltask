﻿using Business.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Interfaces
{
    public interface IQuestionService : ICrud<QuestionModel>
    {
        IEnumerable<QuestionModel> GetAllWithParams(string s, string sort_by, string sort_type, int offset, int limit);
    }
}
