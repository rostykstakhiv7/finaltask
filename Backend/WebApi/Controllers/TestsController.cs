﻿using Business.Interfaces;
using Business.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Filters;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TestsController : ControllerBase
    {
        private readonly ITestService _testService;

        public TestsController(ITestService testService)
        {
            _testService = testService;
        }

        //GET: /api/orders/
        [HttpGet]
        [Authorize(Roles ="Admin")]
        public ActionResult<IEnumerable<TestModel>> Get()
        {
            var result = _testService.GetAll();
            return Ok(result);
        }

        //GET: /api/orders/1
        [HttpGet("{id}")]
        public async Task<ActionResult<TestModel>> GetById(int id)
        {
            try
            {
                var result = await _testService.GetByIdAsync(id);
                return Ok(result);
            }
            catch (ArgumentNullException)
            {
                return NotFound();
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        //POST: /api/orders/
        [HttpPost]
        [Authorize(Roles ="Teacher")]
        public async Task<ActionResult> Add([FromBody] TestModel orderModel)
        {
            try
            {
                await _testService.AddAsync(orderModel);
                return Ok(new { message = "Object was created" });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        //PUT: /api/orders/
        [HttpPut]
        [Authorize(Roles = "Teacher")]
        public async Task<ActionResult> Update(TestModel orderModel)
        {
            await _testService.UpdateAsync(orderModel);
            return Ok();
        }

        //DELETE: /api/orders/1
        [HttpDelete("{id}")]
        [Authorize(Roles = "Teacher")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _testService.DeleteByIdAsync(id);
                return Ok(new { Message = "Object was deleted", StatusCode = 200 });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("taketest")]
        public async Task<ActionResult> TakeTest(int id,int difficulty)
        {
            try
            {
                var test = await _testService.GenerateTestAsync(id,difficulty);
                return Ok(test);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("passtest")]
        public ActionResult PassTest(TestModel test)
        {
            try
            {
                 _testService.CheckTestAsync(test);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
