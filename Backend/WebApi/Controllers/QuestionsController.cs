﻿using Business.Interfaces;
using Business.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using WebApi.Filters;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class QuestionsController : ControllerBase
    {
        private readonly IQuestionService _questionService;

        public QuestionsController(IQuestionService questionService)
        {
            _questionService = questionService;
        }

       

        //GET: /api/lots/
        [HttpGet]
        [AllowAnonymous]
        public ActionResult<IEnumerable<QuestionModel>> Get([FromQuery]string s = "", [FromQuery]string sort_by = "Id", [FromQuery]string sort_type = "asc",
            [FromQuery]int offset = -1, [FromQuery]int limit = -1)
        {
            var count = _questionService.GetAll().Count();
            var result = _questionService.GetAllWithParams(s, sort_by, sort_type, offset,limit);
            return Ok(new { lots = result, count });
        }

        //GET: /api/lots/1
        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<QuestionModel>> GetById(int id)
        {
            try
            {
                var result = await _questionService.GetByIdAsync(id);
                return Ok(result);
            }
            catch(ArgumentNullException)
            {
                return NotFound();
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        //POST: /api/lots/
        [HttpPost]
        public async Task<ActionResult> Add([FromBody] QuestionModel orderModel)
        {
            try
            {
                await _questionService.AddAsync(orderModel);
                return Ok(new { message = "Object was created" });
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
            
        }

        //PUT: /api/lots/
        [HttpPut]
        public async Task<ActionResult> Update([FromBody] QuestionModel orderModel)
        {
            try
            {
                await _questionService.UpdateAsync(orderModel);
                return Ok();
            }
            catch(Exception ex)
            {
                return BadRequest(ex);
            }
        }

        //DELETE: /api/lots/1
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _questionService.DeleteByIdAsync(id);
                return Ok(new { Message = "Object was deleted", StatusCode = 200 });
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
