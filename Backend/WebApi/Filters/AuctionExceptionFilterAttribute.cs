﻿using Business.Validations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;

namespace WebApi.Filters
{
    public class AuctionExceptionFilterAttribute : ExceptionFilterAttribute
    {
        //public Task ExecuteExceptionFilterAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
        //{
        //    if (actionExecutedContext.Exception != null &&
        //            actionExecutedContext.Exception is AuctionException)
        //    {
        //        actionExecutedContext.Response = actionExecutedContext.Request.CreateErrorResponse(
        //        HttpStatusCode.BadRequest, actionExecutedContext.Exception.Message);
        //    }
        //    if (actionExecutedContext.Exception != null &&
        //            actionExecutedContext.Exception is ArgumentNullException)
        //    {
        //        actionExecutedContext.Response = actionExecutedContext.Request.CreateErrorResponse(
        //        HttpStatusCode.NotFound, "Element not found");
        //    }
        //    return Task.FromResult<object>(null);
        //}

        public override void OnException(HttpActionExecutedContext context)
        {
            var exceptionType = context.Exception.GetType();
            if (exceptionType == typeof(AuctionException))
            {
                context.Response.StatusCode = HttpStatusCode.BadRequest;
                context.Response.Content = new StringContent(context.Exception.Message);
            }
            if(exceptionType == typeof(ArgumentNullException))
            {
                context.Response = context.Request.CreateErrorResponse(
                HttpStatusCode.NotFound, "Element not found");
            }
        }
        public override bool AllowMultiple
        {
            get { return true; }
        }

    }
}
